/**
 * ----------------------------------------------------------------------------
 * This file is part of nadaparser.
 * 
 * Copyright 2012 eugen
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version: $
 * @commit: $
 * @author: $
 * @date: $
 * @file: $
 * 
 * @id: $
 * 
 */


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.Deflater;
import java.util.zip.GZIPOutputStream;

/**
 * Helper class used store a pair of language-text String values
 * 
 */
class LocalizedText {
	String language;

	String text;

	public LocalizedText() {
		super();
	}

	public LocalizedText(String language, String text) {
		super();
		this.language = language;
		this.text = text;
	}

};

enum LogEntryType {
	ERROR, INFO, SQLITE, WARNING
}

/**
 * Helper class used to store both the resulted Word and the buffer of the
 * verbose activity
 * 
 */
class ParseResult {
	public Word word;

	public StringBuffer buffer;

	public ParseResult(Word input_word) {
		super();
		this.word = input_word;
		this.buffer = null;
	}
}

/**
 * Helper class used to log the parsing activity into two distinct (compressed)
 * log files: one for the parsing steps and other for the Sqlite insert
 * statements used to populate the destination database
 * 
 */
class ParserLog {

	/*
	 * Is called just before the object is destroyed. Should be called only if
	 * compress_log is true. It will compress the current temporary log then it
	 * will add to the current compressed log file.
	 */
	@Override
	protected void finalize() throws Throwable {
		if (compress_log) {
			compress(log_file, log_file + ".gz");
			new File(log_file).delete();// keep only the .gz log

			compress(log_file_sql, log_file_sql + ".gz");
			new File(log_file_sql).delete();// keep only the .gz log
		}
		super.finalize();
	}

	private String log_file;
	private String log_file_sql;
	protected boolean compress_log;
	protected int max_log_size;

	public ParserLog(String log_file, String log_file_sql, boolean resume) {
		super();

		this.log_file = log_file;
		this.log_file_sql = log_file_sql;

		max_log_size = 1024 * 1024;
		compress_log = true;

		if (!resume) {
			// overwrite existing logs
			new File(log_file).delete();
			new File(log_file_sql).delete();
			// overwrite also the compressed counterparts
			new File(log_file + ".gz").delete();
			new File(log_file_sql + ".gz").delete();
		}
	}

	/**
	 * Compress the srcFile using a GZip algorithm (with best compression
	 * switch) and write the compressed stream to the dstFile
	 * 
	 * @param srcFile
	 *            the source file to be compressed
	 * @param dstFile
	 *            the resulted compressed file
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private void compress(String srcFile, String dstFile) throws FileNotFoundException, IOException {
		FileInputStream in = new FileInputStream(srcFile);
		GZIPOutputStream out = new GZIPOutputStream(new FileOutputStream(dstFile, true)) {
			{
				def.setLevel(Deflater.BEST_COMPRESSION);
			}
		};
		try {
			byte[] buf = new byte[4096];
			int len;
			while ((len = in.read(buf)) > 0)
				out.write(buf, 0, len);
		} finally {
			in.close();
			out.finish();
			out.close();
		}
	}

	/**
	 * Add a new entry to the log_file if the specified buff is valid(it's not
	 * null) but empty. This should occur only if the section is found into html
	 * but it contains no data
	 * 
	 * @param buff
	 *            the String buffer to test
	 * @param section
	 *            the section name that will be written to the log (as being in
	 *            question)
	 */
	public void addLogEntry(StringBuffer buff, String section) {
		if (null != buff && buff.length() == 0)
			addLogEntry(LogEntryType.WARNING, section + " section is EMPTY");
	};

	/**
	 * Write to the log_file (provided to the class constructor) the info and
	 * type provided
	 * 
	 * @param type
	 *            the LogEntryType info to be written
	 * @param info
	 *            the String info to be written
	 */
	public void addLogEntry(LogEntryType type, String info) {
		addLogEntry(log_file, type, info);
	}

	/**
	 * Write to the logFile a new entry as type for the specified
	 * word_id/word_value with the hint provided by info
	 * 
	 * @param type
	 *            the log entry type to be written to the log
	 * @param word_id
	 *            the id of the word for which we add the new entry
	 * @param word_value
	 *            the value of the word for which we add the new entry
	 *            (translated in the original/source language)
	 * @param info
	 *            the hint information about the current entry that has to be
	 *            written to the log
	 */
	public void addLogEntry(LogEntryType type, int word_id, String word_value, String info) {
		writeLogFile(
				log_file,
				String.format("%s  %-7s  %-40s  %-6d  %s\n", new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date()),
						type.toString(), info, word_id, word_value));
	}

	/**
	 * Write to the logFile an entry as type with the info provided
	 * 
	 * @param logFile
	 *            the log file where to append the log entry
	 * @param type
	 *            the entry type to append to the log
	 * @param info
	 *            the String info to write into log
	 */
	private void addLogEntry(String logFile, LogEntryType type, String info) {
		writeLogFile(logFile,
				String.format("%s  %-7s  %s\n", new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date()), type.toString(), info));
	}

	/**
	 * Write to the log_file_sql (provided to the class constructor) the info
	 * and type provided
	 * 
	 * @param type
	 *            the LogEntryType info to be written
	 * @param info
	 *            the String info to be written
	 */
	public void writeSqlLog(LogEntryType type, String info) {
		addLogEntry(log_file_sql, type, info);
	}

	/**
	 * Write the log_data to the logFile
	 * 
	 * @param logFile
	 *            the file where to append the log_data
	 * @param log_data
	 *            the String data to append to logFile
	 */
	private void writeLogFile(String logFile, String log_data) {
		try {
			File f = new File(logFile);
			if (compress_log && f.length() + log_data.length() > max_log_size) {
				compress(logFile, logFile + ".gz");
				f.delete();
			}

			FileWriter filew = new FileWriter(logFile, true);
			BufferedWriter buff = new BufferedWriter(filew);
			buff.write(log_data);
			buff.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

enum WordSection {
	Compound, Definition, Derivation, Example, Explanation, Grammar, Idiom, Inflection, Paradigm, Phonetic, Related, See, Synonym, Translation, Url, Use, Variant, Word, WordComment, WordUrl
}
