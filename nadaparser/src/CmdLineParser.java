/**
 * ----------------------------------------------------------------------------
 * This file is part of lexikon2sqlite/nadaparser.
 * 
 * Copyright 2012 eugen
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version: $
 * @commit: $
 * @author: $
 * @date: $
 * @file: $
 * 
 * @id: $
 * 
 */

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class used to throw an Exception that contains also the argName of the
 * argument that caused the exception
 * 
 */
class CmdLineException extends Exception {

	/**
	 * auto-generated serial version UID for this class
	 */
	private static final long serialVersionUID = 4259399386362036898L;

	/**
	 * The argName of the argument that caused the exception
	 */
	private String argName;

	public CmdLineException(String argName, String message) {
		super(message);
		this.argName = argName;
	}

	/**
	 * Return the name of argument that cause the exception
	 * 
	 * @return the name of the argument
	 */
	public String getArgName() {
		return argName;
	}
}

/**
 * Class that implements the functionality of parsing and validating the program
 * arguments
 * 
 */
class CmdLineParser {
	/**
	 * Class that describe a command line argument
	 * 
	 */
	class Argument {
		/**
		 * the short version for the name of the argument (ex: -o )
		 */
		private String shortName;

		/**
		 * the long version for the name of the argument (ex: --option )
		 */
		private String longName;

		/**
		 * the text that describes the usage of the argument which will be used
		 * to print-out the usage help
		 */
		private String help;

		/**
		 * the value of the argument provided just after its name
		 */
		private String value;

		/**
		 * this is set to true/false by {@link CmdLineParser#parseArguments()}
		 * method if the argument was found among the command line arguments
		 */
		private boolean found;

		/**
		 * true if the argument require a value just after its name
		 */
		private boolean valueRequired;

		/**
		 * a array of strings that represent the allowed values for the argument
		 */
		private String[] validValues;

		/**
		 * true if the argument must to be provided at the command line ; this
		 * is meant for those arguments that are mandatory
		 */
		private boolean mandatory;

		/**
		 * if true then the command for this option will be run even if the
		 * mandatory options do fulfill or not
		 */
		private boolean ignoreMandatory;

		public Argument(String shortName, String longName, boolean valueRequired, boolean mandatory, Object... params) {
			super();
			this.shortName = shortName;
			this.longName = longName;
			this.valueRequired = valueRequired;
			this.mandatory = mandatory;
			this.value = null;
			this.found = false;
			if (null != params) {
				if (params.length > 0)
					this.help = (String) params[0];
				if (params.length > 1)
					this.ignoreMandatory = (Boolean) params[1];
				if (params.length > 2)
					this.validValues = (String[]) params[2];
			}
		}

		protected boolean found() {
			return found;
		}
	}

	/**
	 * Replicate the str String by a number of times specified by times
	 * 
	 * (source: <a
	 * href="http://rosettacode.org/wiki/Repeat_a_string#Java">Rosetta Code</a>)
	 * 
	 * @param str
	 *            string to replicate
	 * @param times
	 *            number of times to replicate that string
	 * @return the replicated string
	 */
	private static String repeat(String str, int times) {
		return new String(new char[times]).replace("\0", str);
	}

	/**
	 * the name/email of the author that will be displayed in the help
	 */
	private String authorContact;
	/**
	 * the list of the arguments we define and expect to be provided at the
	 * command line
	 */
	private List<Argument> argList;

	/**
	 * the array of arguments provided for the program
	 */
	private String[] cmdLineArgs;

	public CmdLineParser(String[] args, String authorContact) {
		super();
		argList = new ArrayList<Argument>();
		cmdLineArgs = args;
		this.authorContact = authorContact;
	}

	/**
	 * Add a new Argument definition to the argList
	 * 
	 * @param shortName
	 *            the short name of the argument (ex: -o)
	 * @param longName
	 *            the long name of the argument (ex: --option)
	 * @param valueRequired
	 *            true if the argument should be followed by its value
	 * @param mandatory
	 *            true if the argument must be provided at the command line
	 * @param params
	 *            an variable array of Object arguments that can be passed to
	 *            this method; in this version we expect only one element which
	 *            in fact is the text that describes the usage of the argument
	 *            that will be used to print-out the argument's usage help
	 */
	protected void add(String shortName, String longName, boolean valueRequired, boolean mandatory, Object... params) {
		argList.add(new Argument(shortName, longName, valueRequired, mandatory, params));
	}

	/**
	 * Check whether exists or not at least one argument which has the property
	 * {@link Argument#ignoreMandatory} set to true
	 * 
	 * Such an option could be used if you want to define an option like --help
	 * and you don't want to get an error message which says that: you have not
	 * provided whatever parameter, ex: -i
	 * 
	 * @return true if there exists defined a such parameter
	 */
	private boolean existsIgnoreArgument() {
		boolean result = false;
		for (int i = 0; i < argList.size(); i++)
			if (argList.get(i).found && argList.get(i).ignoreMandatory) {
				result = true;
				break;
			}
		return result;
	}

	/**
	 * Search in the {@link #argList} for that argument whose name is like the
	 * one provided here
	 * 
	 * @param name
	 *            the name of the argument to search for
	 * @param byShortName
	 *            true if the provided argument name is short otherwise we
	 *            consider that the name is long
	 * @return the Argument object for the specified name
	 */
	protected Argument getArgByName(String name, boolean byShortName) {
		Argument result = null;
		for (int i = 0; i < argList.size(); i++)
			if ((byShortName ? argList.get(i).shortName : argList.get(i).longName).compareTo(name) == 0) {
				result = argList.get(i);
				break;
			}
		return result;
	}

	/**
	 * Search in {@link #argList} for that argument whose long name is equal
	 * with name and if found then returns its value, as specified at the
	 * command line
	 * 
	 * @param name
	 *            the long name of the argument to search for
	 * @return a String containing the argument value as provided at command
	 *         line
	 */
	public String getArgValueByLongName(String name) {
		return getArgByName(name, false).value;
	}

	/**
	 * Search in {@link #argList} for that argument whose short name is equal
	 * with name and if found then returns its value, as specified at the
	 * command line
	 * 
	 * @param name
	 *            the short name of the argument to search for
	 * @return a String containing the argument value as provided at command
	 *         line
	 */
	public String getArgValueByShortName(String name) {
		return getArgByName(name, true).value;
	}

	/**
	 * Returns a string that represents the {@link Argument#validValues} that
	 * will be shown in the help output for the specified arg argument
	 * 
	 * @param arg
	 *            the argument for which determine and return this string
	 * @return a string representing the value example as will be shown in the
	 *         help output for the specified argument
	 */
	private String getHelpArgValue(Argument arg) {
		if (arg.validValues != null && arg.validValues.length > 0) {
			String result = "";
			for (int i = 0; i < arg.validValues.length; i++)
				result += (i > 0 ? "|" : "") + arg.validValues[i];
			return "=" + (result.indexOf("|") > 0 ? "<" + result + ">" : result);
		} else if (arg.valueRequired)
			return "=<value>";
		else
			return "";
	}

	/**
	 * This function returns the maximum length of any values example that are
	 * shown in the help output It is used internally by
	 * {@link #printHelp(String, String)} to know how much spaces should be
	 * printed from the argument's value example to the help message
	 * 
	 * @return
	 */
	private int getHelpArgValueMaxLen() {
		int helpArgLen;
		int max = 0;
		for (Argument a : argList) {
			helpArgLen = getHelpArgValue(a).length();
			max = max > helpArgLen ? max : helpArgLen;
		}
		return max;
	}

	/**
	 * Get the maximum length of argument's long names
	 * 
	 * @return an integer representing the maximum length of argument's names
	 */
	private int getLongArgNameMaxLen() {
		int max = 0;
		for (int i = 0; i < argList.size(); i++)
			max = argList.get(i).longName.length() > max ? argList.get(i).longName.length() : max;
		return max;
	}

	/**
	 * Return the application main-class-name
	 * 
	 * @return the name of the main-class of the program; this should always be
	 *         modified accordingly with your main-class
	 */
	private String getMainClassName() {

		return Main.class.getName();

	}

	/**
	 * Return the application .jar name, if it's run from within a .jar file
	 * 
	 * @return the name of running .jar file
	 */
	private String getRunningJarName() {
		return null;
	}

	/**
	 * Method to be called in order to parse the arguments provided at the
	 * application command line. This should be called after you have already
	 * defined the application command line arguments.
	 * 
	 * @throws Exception
	 */
	public void parseArguments() throws Exception {
		int i = 0, k = -1;
		while (i < cmdLineArgs.length) {
			if (cmdLineArgs[i].startsWith("-")) {
				boolean longFormat = (cmdLineArgs[i].length() > 2 && cmdLineArgs[i].charAt(1) == '-');
				String arg = cmdLineArgs[i].substring(1 + (longFormat ? 1 : 0));
				if (longFormat) {
					k = arg.indexOf("=");
					if (k > -1)
						arg = arg.substring(0, k);
				}
				Argument cmdArg = null;
				for (int j = 0; j < argList.size(); j++)
					if ((longFormat ? argList.get(j).longName : argList.get(j).shortName).compareTo(arg) == 0) {
						cmdArg = argList.get(j);
						break;
					}
				if (null == cmdArg)
					throw new CmdLineException(arg, "Invalid argument " + cmdLineArgs[i]);
				else {
					cmdArg.found = true;
					if (cmdArg.valueRequired) {
						if (!longFormat && i + 1 >= cmdLineArgs.length || !longFormat && cmdLineArgs[i + 1].startsWith("-") || longFormat
								&& k < 0)
							throw new CmdLineException(arg, "Value required for argument " + cmdLineArgs[i]);
						else {
							cmdArg.value = longFormat ? cmdLineArgs[i].substring(k + 3) : cmdLineArgs[i + 1];
							if (cmdArg.validValues != null && !validCmdArgValues(cmdArg)) {
								String v = "";
								for (String s : cmdArg.validValues)
									v += (v.length() > 0 ? "," : "") + s;
								throw new CmdLineException(arg, "Argument's value for option -" + cmdArg.shortName
										+ " should be one of the following: " + v);
							}

						}
					}
				}
			}
			i++;
		}

		Argument argNotFound = null;
		for (int j = 0; j < argList.size(); j++)
			if (argList.get(j).mandatory && !argList.get(j).found) {
				argNotFound = argList.get(j);
				break;
			}
		if (!existsIgnoreArgument() && null != argNotFound)
			throw new CmdLineException(argNotFound.shortName, "\nERROR : Mandatory argument not found (ex: -" + argNotFound.shortName
					+ " )");
	}

	/**
	 * Show the usage help which is generated automatically based on the
	 * arguments definition and the .jar/main-class name
	 * 
	 * @param message
	 *            the message to be shown just one line above the usage help
	 *            (ex: the reason for showing the help, like "invalid parameter"
	 *            or something like that)
	 */
	public void printHelp(String message, String appVersion) {
		String jarName = getRunningJarName();

		if (null == jarName)
			jarName = getMainClassName();
		else
			jarName = "-jar " + jarName;

		if (null != message && message.length() > 0)
			System.out.println(message + "\n");
		System.out.println(appVersion);
		System.out.println(String.format("Usage: java %s [-options]\nwhere options include:", jarName));
		for (Argument a : argList) {
			System.out.printf("  -%s,  --%s%s%s; (%s) %s\n", a.shortName, a.longName, getHelpArgValue(a),
					repeat(" ", 1 + getLongArgNameMaxLen() - a.longName.length() + getHelpArgValueMaxLen() - getHelpArgValue(a).length()),
					a.mandatory ? "mandatory" : "optional", null == a.help ? "" : a.help);
		}

		if (null != authorContact && authorContact.length() > 0)
			System.out.println(String.format("\nReport bugs to %s.", authorContact));
	}

	/**
	 * Check if the value supplied for the cmdArg argument matches those that
	 * were defined as {@link Argument#validValues}
	 * 
	 * @param cmdArg
	 *            the argument to be checked
	 * @return true if the value supplied for the argument matches one of these
	 *         values specified in the validValues property, otherwise returns
	 *         false
	 */
	private boolean validCmdArgValues(Argument cmdArg) {
		boolean result = cmdArg.validValues.length == 0;
		for (int i = 0; i < cmdArg.validValues.length; i++)
			if (cmdArg.value.compareTo(cmdArg.validValues[i]) == 0) {
				result = true;
				break;
			}
		return result;
	}
}
