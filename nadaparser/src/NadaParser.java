/**
 * ----------------------------------------------------------------------------
 * This file is part of nadaparser.
 * 
 * Copyright 2012 eugen
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version: $
 * @commit: $
 * @author: $
 * @date: $
 * @file: $
 * 
 * @id: $
 * 
 */


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.w3c.dom.Node;

/**
 * Class used to parse a html document (that encapsulates only those DIV
 * elements corresponding to only one word) and to return in exchange the
 * corresponding Word object
 * 
 */
public class NadaParser {

	private Set<WordSection> sections;

	private Set<WordSection> duplicate_sections;

	private ParserLog log;

	private boolean verbose;

	final String crlf = System.getProperty("line.separator");

	public NadaParser(ParserLog log, boolean verbose, Set<WordSection> sections, Set<WordSection> duplicate_sections) {
		super();
		this.log = log;
		this.sections = sections;
		this.duplicate_sections = duplicate_sections;
		this.verbose = verbose;
	}

	/**
	 * Extract the grammar rules
	 * 
	 * @param node
	 *            the DIV where we start to scan for grammar rules
	 * @return an array with 2 elements: first for the grammar rule in the
	 *         source language, second the rule in the translated language (or a
	 *         second rule in the source language)
	 */
	private LocalizedText[] getGrammarRule(Node node) {
		LocalizedText[] result = new LocalizedText[2];

		String left_rule = node.getChildNodes().item(0).getNodeValue();
		LocalizedText lt = getNodeLocalText(node.getChildNodes().item(1));
		String right_rule = node.getChildNodes().item(2).getNodeValue();

		if (null != lt)
			result[0] = new LocalizedText(lt.language, left_rule + lt.text + right_rule);

		if (node.getChildNodes().getLength() > 4) {
			lt = getNodeLocalText(node.getChildNodes().item(3));
			right_rule = node.getChildNodes().item(4).getNodeValue();

			if (null != lt)
				result[1] = new LocalizedText(lt.language, left_rule + lt.text + right_rule);

		}

		return result;
	}

	/**
	 * Extract a list of LI items from the specified DIV node
	 * 
	 * @param node
	 *            the DIV node where we start to scan
	 * @return a list of 2-elements array for all LI items found under node:
	 *         first element for the text expressed in the source language,
	 *         second for the text expressed in the translated language
	 */
	private List<LocalizedText[]> getListLocalText(Node node) {
		List<LocalizedText[]> result = new ArrayList<LocalizedText[]>();
		for (int i = 0; i < node.getChildNodes().getLength(); i++) {
			Node li = node.getChildNodes().item(i);
			if (li.getNodeName().compareToIgnoreCase("LI") == 0) {
				LocalizedText[] lta = new LocalizedText[2];
				int k = 0;
				for (int j = 0; j < li.getChildNodes().getLength(); j++) {
					LocalizedText lt = getNodeLocalText(li.getChildNodes().item(j));
					if (null != lt)
						lta[k++] = lt;
				}
				result.add(lta);
			}
		}
		return result;
	}

	/**
	 * Extract the text (and the language code) for the specified node (usually
	 * a SPAN element)
	 * 
	 * @param node
	 *            the node that represent the element where to extract the text
	 * @return an object that contain the text and the language code of the
	 *         specified node
	 */
	private LocalizedText getNodeLocalText(Node node) {
		LocalizedText result = new LocalizedText();
		if (null != node.getAttributes())
			for (int i = 0; i < node.getAttributes().getLength(); i++)
				if (node.getAttributes().item(i).getNodeName().compareToIgnoreCase("lang") == 0) {
					result.language = node.getAttributes().item(i).getNodeValue();
					break;
				}
		if (null != node.getChildNodes())
			for (int i = 0; i < node.getChildNodes().getLength(); i++)
				if (node.getChildNodes().item(i).getNodeName().compareToIgnoreCase("#text") == 0) {
					result.text = node.getChildNodes().item(i).getNodeValue();
					break;
				}
		return (null != result.language ? result.language.length() : 0) + (null != result.text ? result.text.length() : 0) == 0 ? null
				: result;
	}

	/**
	 * Extract a list of items that starts and ends with a prefix/suffix (within
	 * a "#text" element); values are stored in the SPAN items
	 * 
	 * @param node
	 *            the parent node that contains the list of SPAN items
	 * @param prefix
	 *            the prefix to be used in order to identify the section (regex
	 *            allowed)
	 * @param suffix
	 *            the suffix to be used in order to identify the section (regex
	 *            allowed)
	 * @param allowSimpleForm
	 *            if true then it is supposed that the section could not have
	 *            prefix/suffix
	 * @return a list of LocalizedText containing all elements found
	 */
	private List<LocalizedText> getSetLocalText(Node node, String prefix, String suffix, boolean allowSimpleForm) {
		List<LocalizedText> result = new ArrayList<LocalizedText>();
		int childCount = node.getChildNodes().getLength();

		if (allowSimpleForm
				&& childCount == 1
				&& node.getChildNodes().item(0).getChildNodes().getLength() > 0
				|| (childCount > 1 && node.getChildNodes().item(0).getNodeName().compareToIgnoreCase("#text") == 0
						&& node.getChildNodes().item(0).getNodeValue().trim().matches(prefix)
						&& node.getChildNodes().item(childCount - 1).getNodeName().compareToIgnoreCase("#text") == 0 && node
						.getChildNodes().item(childCount - 1).getNodeValue().trim().matches(suffix)))
			for (int i = 0; i < childCount; i++)
				if (node.getChildNodes().item(i).getNodeName().compareToIgnoreCase("SPAN") == 0) {
					LocalizedText lt = getNodeLocalText(node.getChildNodes().item(i));
					if (null != lt)
						result.add(lt);
				}
		return result;
	}

	/**
	 * Scan the node if looks like a NADA section
	 * 
	 * @param node
	 *            the node where to start scanning
	 * @param section
	 *            the name of the section we search for
	 * @return true if the node represents a section having the specified name
	 */
	private boolean isSection(Node node, String section) {
		if (node.getNodeName().compareToIgnoreCase("P") == 0 && node.getChildNodes().getLength() > 0) {
			LocalizedText lt = getNodeLocalText(node.getChildNodes().item(0));
			if (null != lt && lt.text.startsWith(section))
				return true;
		}
		return false;
	}

	/**
	 * Parse the specified DIV node and return a Word object containing all the
	 * sections found
	 * 
	 * @param div
	 *            the DIV element to be scanned
	 * @param initWord
	 *            the initial Word information; by calling this method
	 *            consecutively using the same initWord we get in the end a Word
	 *            object that has collected all the possible data
	 * @return a Word object containing all the information provided by the
	 *         initial Word object + those information found at the div Node
	 */
	protected Word parse(Node div, Word initWord) {

		div = div.getFirstChild();

		ParseResult pr = parseWord(div, initWord);
		log.addLogEntry(pr.buffer, "word/phonetic");

		pr = parseWordComment(div, pr.word);
		log.addLogEntry(pr.buffer, "word comment");

		pr = parseInflection(div, pr.word);
		log.addLogEntry(pr.buffer, "inflection");

		pr = parseDefinitionExplanation(div, pr.word);
		log.addLogEntry(pr.buffer, "definition");

		pr = parseWordTranslation(div, pr.word);
		log.addLogEntry(pr.buffer, "translation");

		pr = parseUse(div, pr.word);
		log.addLogEntry(pr.buffer, "use");

		pr = parseVariant(div, pr.word);
		log.addLogEntry(pr.buffer, "variant");

		pr = parseDerivation(div, pr.word);
		log.addLogEntry(pr.buffer, "derivation");

		pr = parseGrammar(div, pr.word);
		log.addLogEntry(pr.buffer, "grammar");

		pr = parsePicture(div, pr.word);
		log.addLogEntry(pr.buffer, "picture");

		pr = parseExample(div, pr.word);
		log.addLogEntry(pr.buffer, "example");

		pr = parseIdiom(div, pr.word);
		log.addLogEntry(pr.buffer, "idiom");

		pr = parseCompound(div, pr.word);
		log.addLogEntry(pr.buffer, "compound");

		pr = parseRelated(div, pr.word);
		log.addLogEntry(pr.buffer, "related");

		return pr.word;
		// TODO de facut parser pt word parts (see bord|lägger)
	}

	/**
	 * Parse the specified DIV node and if Compound section found then fill the
	 * word with corresponding info
	 * 
	 * @param div
	 *            the DIV node to be parsed
	 * @param word
	 *            the Word containing the previous Word data parsed/found
	 * @return an object containing the previous Word + current Compound section
	 *         data
	 */
	private ParseResult parseCompound(Node div, Word word) {
		ParseResult result = new ParseResult(word);

		if (null != div && null != div.getChildNodes() && div.getChildNodes().getLength() == 2
				&& isSection(div.getChildNodes().item(0), "Sammansättningar")) {

			List<LocalizedText[]> li = getListLocalText(div.getChildNodes().item(1));
			if (li.size() > 0) {
				result.buffer = new StringBuffer();

				if (sections.contains(WordSection.Compound))
					duplicate_sections.add(WordSection.Compound);

				for (int i = 0; i < li.size(); i++) {
					if (null != li.get(i)[0]) {
						result.buffer.append(String.valueOf(i + 1) + ") compound[" + li.get(i)[0].language + "].value=" + li.get(i)[0].text
								+ crlf);
						result.word.compound.add(new Compound("", li.get(i)[0].text));
					}

					if (null != li.get(i)[1]) {
						result.buffer
								.append(String.valueOf(i + 1) + ") compound[" + li.get(i)[1].language + "].value=" + li.get(i)[1].text);
						result.word.compound.get(result.word.compound.size() - 1).translation.add(new Translation("", li.get(i)[1].text));
					}

					if (!sections.contains(WordSection.Compound))
						sections.add(WordSection.Compound);
				}
				verboseOut(result.buffer);
			}
		}
		return result;
	}

	/**
	 * Parse the specified DIV node and if Definition/Explanation section found
	 * then fill the word with corresponding info
	 * 
	 * @param div
	 *            the DIV node to be parsed
	 * @param word
	 *            the Word containing the previous Word data parsed/found
	 * @return an object containing the previous Word + current
	 *         Definition/Explanation section data
	 */
	private ParseResult parseDefinitionExplanation(Node div, Word word) {
		ParseResult result = new ParseResult(word);
		boolean isDefinition = true;
		boolean printBuffer = false;
		final String[] sectionType = { "explanation", "definition" };

		List<LocalizedText> slt = getSetLocalText(div, "\\(", "\\)", true);
		if (slt.size() > 0) {
			result.buffer = new StringBuffer();

			// sometimes definition/explanation have the same pattern;
			// we assume that if definition section does not exist then the
			// section should describe 'a definition'
			if (!sections.contains(WordSection.Definition)) {
				if (sections.contains(WordSection.Definition))
					duplicate_sections.add(WordSection.Definition);
				sections.add(WordSection.Definition);
				result.word.definition.add(new Definition(slt.get(0).text));
				printBuffer = true;
			}
			// otherwise the section should describe 'an explanation'
			else {
				// sometimes comment and explanation have the same pattern
				if (null != word.comment && word.comment.compareToIgnoreCase(slt.get(0).text) != 0) {
					if (sections.contains(WordSection.Explanation))
						duplicate_sections.add(WordSection.Explanation);
					sections.add(WordSection.Explanation);
					word.explanation.add(new Explanation(slt.get(0).text));
					printBuffer = true;
				}
				isDefinition = false;
			}

			if (printBuffer)
				result.buffer.append(sectionType[isDefinition ? 1 : 0] + "[" + slt.get(0).language + "].value=" + slt.get(0).text + crlf);

			if (slt.size() > 1) {
				if (printBuffer)
					result.buffer.append(sectionType[isDefinition ? 1 : 0] + "[" + slt.get(1).language + "].value=" + slt.get(1).text
							+ crlf);
				if (isDefinition) {
					result.word.definition.get(0).translation.add(new Translation("", slt.get(1).text));
				} else {
					if (result.word.explanation.size() > 0) {
						result.word.explanation.get(0).translation.add(new Translation("", slt.get(1).text));
					}
				}
			}

			verboseOut(result.buffer);
		}
		return result;
	}

	/**
	 * Parse the specified DIV node and if Derivation section found then fill
	 * the word with corresponding info
	 * 
	 * @param div
	 *            the DIV node to be parsed
	 * @param word
	 *            the Word containing the previous Word data parsed/found
	 * @return an object containing the previous Word + current Derivation
	 *         section data
	 */
	private ParseResult parseDerivation(Node div, Word word) {
		ParseResult result = new ParseResult(word);

		if (null != div && null != div.getChildNodes() && div.getChildNodes().getLength() == 2
				&& isSection(div.getChildNodes().item(0), "Avledningar")) {

			List<LocalizedText[]> li = getListLocalText(div.getChildNodes().item(1));
			if (li.size() > 0) {
				result.buffer = new StringBuffer();

				if (sections.contains(WordSection.Derivation))
					duplicate_sections.add(WordSection.Derivation);

				for (int i = 0; i < li.size(); i++) {

					if (null != li.get(i)[0]) {
						result.buffer.append(String.valueOf(i + 1) + ") derivation[" + li.get(i)[0].language + "].value="
								+ li.get(i)[0].text + crlf);
						result.word.derivation.add(new Derivation(li.get(i)[0].text));
					}

					if (null != li.get(i)[1]) {
						result.buffer.append(String.valueOf(i + 1) + ") derivation[" + li.get(i)[1].language + "].value="
								+ li.get(i)[1].text + crlf);
						result.word.derivation.get(result.word.derivation.size() - 1).translation
								.add(new Translation("", li.get(i)[1].text));
					}

					if (!sections.contains(WordSection.Derivation))
						sections.add(WordSection.Derivation);
				}
				verboseOut(result.buffer);
			}
		}
		return result;
	}

	/**
	 * Parse the specified DIV node and if Example section found then fill the
	 * word with corresponding info
	 * 
	 * @param div
	 *            the DIV node to be parsed
	 * @param word
	 *            the Word containing the previous Word data parsed/found
	 * @return an object containing the previous Word + current Example section
	 *         data
	 */
	private ParseResult parseExample(Node div, Word word) {
		ParseResult result = new ParseResult(word);

		if (null != div && null != div.getChildNodes() && div.getChildNodes().getLength() == 2
				&& isSection(div.getChildNodes().item(0), "Exempel")) {

			List<LocalizedText[]> li = getListLocalText(div.getChildNodes().item(1));
			if (li.size() > 0) {

				result.buffer = new StringBuffer();

				if (sections.contains(WordSection.Example))
					duplicate_sections.add(WordSection.Example);

				for (int i = 0; i < li.size(); i++) {

					if (null != li.get(i)[0]) {
						result.buffer.append(String.valueOf(i + 1) + ") example[" + li.get(i)[0].language + "].value=" + li.get(i)[0].text
								+ crlf);
						result.word.example.add(new Example(li.get(i)[0].text));
					}

					if (null != li.get(i)[1]) {
						result.buffer.append(String.valueOf(i + 1) + ") example[" + li.get(i)[1].language + "].value=" + li.get(i)[1].text
								+ crlf);
						result.word.example.get(result.word.example.size() - 1).translation.add(new Translation("", li.get(i)[1].text));
					}

					if (!sections.contains(WordSection.Example))
						sections.add(WordSection.Example);
				}
				verboseOut(result.buffer);
			}
		}
		return result;
	}

	/**
	 * Parse the specified DIV node and if Grammar section found then fill the
	 * word with corresponding info
	 * 
	 * @param div
	 *            the DIV node to be parsed
	 * @param word
	 *            the Word containing the previous Word data parsed/found
	 * @return an object containing the previous Word + current Grammar section
	 *         data
	 */
	private ParseResult parseGrammar(Node div, Word word) {
		// TODO multiple grammar rule
		ParseResult result = new ParseResult(word);

		if (null != div && null != div.getChildNodes() && div.getChildNodes().getLength() == 1
				&& div.getChildNodes().item(0).getNodeName().compareToIgnoreCase("A") == 0
				&& div.getChildNodes().item(0).getChildNodes().getLength() >= 3) {

			LocalizedText[] gr = getGrammarRule(div.getChildNodes().item(0));

			result.buffer = new StringBuffer();

			if (sections.contains(WordSection.Grammar))
				duplicate_sections.add(WordSection.Grammar);

			result.buffer.append("grammar[" + gr[0].language + "].value=" + gr[0].text + (null != gr[1] ? gr[1].text : "") + crlf);
			result.word.grammar.add(new Grammar("", gr[0].text + (null != gr[1] ? gr[1].text : "")));

			verboseOut(result.buffer);

			sections.add(WordSection.Grammar);
		}
		return result;
	}

	/**
	 * Parse the specified DIV node and if Idiom section found then fill the
	 * word with corresponding info
	 * 
	 * @param div
	 *            the DIV node to be parsed
	 * @param word
	 *            the Word containing the previous Word data parsed/found
	 * @return an object containing the previous Word + current Idiom section
	 *         data
	 */
	private ParseResult parseIdiom(Node div, Word word) {
		ParseResult result = new ParseResult(word);

		if (null != div && null != div.getChildNodes() && div.getChildNodes().getLength() == 2
				&& isSection(div.getChildNodes().item(0), "Uttryck")) {

			List<LocalizedText[]> li = getListLocalText(div.getChildNodes().item(1));
			if (li.size() > 0) {

				result.buffer = new StringBuffer();

				if (sections.contains(WordSection.Idiom))
					duplicate_sections.add(WordSection.Idiom);

				for (int i = 0; i < li.size(); i++) {

					if (null != li.get(i)[0]) {
						result.buffer.append(String.valueOf(i + 1) + ") idiom[" + li.get(i)[0].language + "].value=" + li.get(i)[0].text
								+ crlf);
						result.word.idiom.add(new Idiom(li.get(i)[0].text));
					}

					if (null != li.get(i)[1]) {
						result.buffer.append(String.valueOf(i + 1) + ") idiom[" + li.get(i)[1].language + "].value=" + li.get(i)[1].text
								+ crlf);
						result.word.idiom.get(result.word.idiom.size() - 1).translation.add(new Translation("", li.get(i)[1].text));
					}

					if (!sections.contains(WordSection.Idiom))
						sections.add(WordSection.Idiom);
				}
				verboseOut(result.buffer);
			}
		}
		return result;
	}

	/**
	 * Parse the specified DIV node and if Inflection section found then fill
	 * the word with corresponding info
	 * 
	 * @param div
	 *            the DIV node to be parsed
	 * @param word
	 *            the Word containing the previous Word data parsed/found
	 * @return an object containing the previous Word + current Inflection
	 *         section data
	 */
	private ParseResult parseInflection(Node div, Word word) {
		ParseResult result = new ParseResult(word);

		List<LocalizedText> slt = getSetLocalText(div, "〈", "〉", false);
		if (slt.size() > 0) {
			result.buffer = new StringBuffer();

			if (sections.contains(WordSection.Inflection))
				duplicate_sections.add(WordSection.Inflection);

			for (int i = 0; i < slt.size(); i++) {
				result.buffer.append("inflection[" + slt.get(i).language + "].value=" + slt.get(i).text + crlf);
				result.word.inflection.add(new Inflection("", slt.get(i).text));

				if (!sections.contains(WordSection.Inflection))
					sections.add(WordSection.Inflection);
			}

			verboseOut(result.buffer);
		}

		return result;
	}

	/**
	 * Parse the specified DIV node and if Picture(Url) section found then fill
	 * the word with corresponding info
	 * 
	 * @param div
	 *            the DIV node to be parsed
	 * @param word
	 *            the Word containing the previous Word data parsed/found
	 * @return an object containing the previous Word + current Picture(Url)
	 *         section data
	 */
	private ParseResult parsePicture(Node div, Word word) {
		// TODO daca este mai mult de 1 pereche de poze atunci sa extraga si
		// urmatoarele perechi de poze (ex: duk)
		ParseResult result = new ParseResult(word);

		if (null != div
				&& null != div.getChildNodes()
				&& div.getChildNodes().getLength() >= 1
				&& div.getChildNodes().item(0).getNodeName().compareToIgnoreCase("A") == 0
				&& (div.getChildNodes().getLength() == 1 && div.getChildNodes().item(0).getChildNodes().getLength() == 1 || div
						.getChildNodes().getLength() >= 3
						&& div.getChildNodes().item(1).getNodeName().compareToIgnoreCase("#text") == 0
						&& div.getChildNodes().item(2).getNodeName().compareToIgnoreCase("A") == 0)) {

			result.buffer = new StringBuffer();

			String url_original = div.getChildNodes().item(0).getAttributes().item(0).getNodeValue();
			String url_translated = "";
			if (div.getChildNodes().getLength() > 1)
				url_translated = div.getChildNodes().item(2).getAttributes().item(0).getNodeValue();

			if (sections.contains(WordSection.Url))
				duplicate_sections.add(WordSection.Url);

			result.buffer.append("picture[original].url=" + url_original + crlf);
			result.buffer.append("picture[translated].url=" + url_translated + crlf);
			result.word.url.add(new URL("any", url_original));

			verboseOut(result.buffer);

			sections.add(WordSection.Url);
		}
		return result;
	}

	/**
	 * Parse the specified DIV node and if Related section found then fill the
	 * word with corresponding info
	 * 
	 * @param div
	 *            the DIV node to be parsed
	 * @param word
	 *            the Word containing the previous Word data parsed/found
	 * @return an object containing the previous Word + current Related section
	 *         data
	 */
	private ParseResult parseRelated(Node div, Word word) {
		ParseResult result = new ParseResult(word);

		if (null != div && null != div.getChildNodes() && div.getChildNodes().getLength() == 1
				&& div.getChildNodes().item(0).getNodeName().compareToIgnoreCase("#text") == 0) {

			result.buffer = new StringBuffer();

			String related = div.getChildNodes().item(0).getNodeValue().replace("Motsatser:", "").trim();

			if (sections.contains(WordSection.Related))
				duplicate_sections.add(WordSection.Related);

			result.buffer.append("related.value=" + related + crlf);
			result.word.related.add(new Related("", "antonym", related));
			verboseOut(result.buffer);
			sections.add(WordSection.Related);
		}
		return result;
	}

	/**
	 * Parse the specified DIV node and if Use section found then fill the word
	 * with corresponding info
	 * 
	 * @param div
	 *            the DIV node to be parsed
	 * @param word
	 *            the Word containing the previous Word data parsed/found
	 * @return an object containing the previous Word + current Use section data
	 */
	private ParseResult parseUse(Node div, Word word) {
		ParseResult result = new ParseResult(word);

		if (null != div && null != div.getChildNodes() && div.getChildNodes().getLength() >= 2) {
			LocalizedText title = getNodeLocalText(div.getChildNodes().item(0));
			if (null != title && title.text.startsWith("Användning")) {
				result.buffer = new StringBuffer();

				if (sections.contains(WordSection.Use))
					duplicate_sections.add(WordSection.Use);

				for (int i = 1; i < div.getChildNodes().getLength(); i++) {
					LocalizedText lt = getNodeLocalText(div.getChildNodes().item(i));
					if (null != lt) {
						result.buffer.append(String.valueOf(i + 1) + ") use[" + lt.language + "].value=" + lt.text + crlf);
						result.word.use.add(new Use(lt.text));
					}
				}

				if (!sections.contains(WordSection.Use))
					sections.add(WordSection.Use);

				verboseOut(result.buffer);

			}
		}
		return result;
	}

	/**
	 * Parse the specified DIV node and if Variant section found then fill the
	 * word with corresponding info
	 * 
	 * @param div
	 *            the DIV node to be parsed
	 * @param word
	 *            the Word containing the previous Word data parsed/found
	 * @return an object containing the previous Word + current Variant section
	 *         data
	 */
	private ParseResult parseVariant(Node div, Word word) {
		ParseResult result = new ParseResult(word);

		if (null != div && null != div.getChildNodes() && div.getChildNodes().getLength() >= 2)
			if (div.getChildNodes().item(0).getNodeName().compareToIgnoreCase("#text") == 0
					&& div.getChildNodes().item(0).getNodeValue().startsWith("Variantform")) {

				result.buffer = new StringBuffer();

				if (sections.contains(WordSection.Variant))
					duplicate_sections.add(WordSection.Variant);

				for (int i = 1; i < div.getChildNodes().getLength(); i++)
					if (div.getChildNodes().item(i).getNodeName().compareToIgnoreCase("SPAN") == 0) {
						LocalizedText lt = getNodeLocalText(div.getChildNodes().item(i));
						if (null != lt) {
							result.buffer.append(String.valueOf(result.word.variant.size() + 1) + ") variant[" + lt.language + "].value="
									+ lt.text + crlf);
							result.word.variant.add(new Variant("", "", lt.text));
						}
						if (!sections.contains(WordSection.Variant))
							sections.add(WordSection.Variant);
					}
				verboseOut(result.buffer);
			}
		return result;
	}

	/**
	 * Parse the specified DIV node and if basic word data found then fill the
	 * word with corresponding info
	 * 
	 * @param div
	 *            the DIV node to be parsed
	 * @param word
	 *            the Word containing the previous Word data parsed/found
	 * @return an object containing the previous Word + current basic word data
	 *         (such as word value, word class, phonetic, word url)
	 */
	private ParseResult parseWord(Node div, Word word) {
		ParseResult result = new ParseResult(word);
		if (null != div && null != div.getChildNodes() && div.getChildNodes().getLength() == 4)
			if (div.getChildNodes().item(0).getNodeName().compareToIgnoreCase("B") == 0
					&& div.getChildNodes().item(1).getNodeName().compareToIgnoreCase("#text") == 0
					&& div.getChildNodes().item(2).getNodeName().compareToIgnoreCase("A") == 0
					&& div.getChildNodes().item(3).getNodeName().compareToIgnoreCase("#text") == 0) {

				result.buffer = new StringBuffer();

				String lang = div.getChildNodes().item(0).getChildNodes().item(0).getAttributes().item(0).getNodeValue();
				String class_ = div.getChildNodes().item(3).getNodeValue();

				String value = div.getChildNodes().item(0).getChildNodes().item(0).getChildNodes().item(0).getNodeValue();
				result.buffer.append("word.value[" + lang + "]=" + value + crlf);

				result.word.value = value.trim();
				result.word.class_ = class_.trim();
				result.word.lang = lang.trim();

				result.buffer.append("word.class_=" + class_ + crlf);

				String phonetic = div.getChildNodes().item(1).getNodeValue().trim();
				result.buffer.append("phonetic.value=" + phonetic + crlf);

				String url = div.getChildNodes().item(2).getAttributes().item(0).getNodeValue().trim();
				result.buffer.append("url.value=" + url + crlf);
				result.word.phonetic.add(new Phonetic(url, phonetic));

				if (sections.contains(WordSection.Word))
					duplicate_sections.add(WordSection.Word);

				sections.add(WordSection.Word);
				sections.add(WordSection.Phonetic);
				sections.add(WordSection.WordUrl);

				verboseOut(result.buffer);
			}
		return result;
	}

	/**
	 * Parse the specified DIV node and if word comment element found then fill
	 * the word with corresponding info
	 * 
	 * @param div
	 *            the DIV node to be parsed
	 * @param word
	 *            the Word containing the previous Word data parsed/found
	 * @return an object containing the previous Word + current word comment
	 *         element info
	 */
	private ParseResult parseWordComment(Node div, Word word) {
		ParseResult result = new ParseResult(word);
		final String openTag = "(";
		final String closeTag = ")";
		if (null != div && null != div.getChildNodes() && div.getChildNodes().getLength() >= 2)
			if (div.getChildNodes().item(0).getNodeName().compareToIgnoreCase("#text") == 0
					&& div.getChildNodes().item(div.getChildNodes().getLength() - 1).getNodeName().compareToIgnoreCase("#text") == 0
					&& div.getChildNodes().item(0).getNodeValue().trim().compareToIgnoreCase(openTag) == 0
					&& div.getChildNodes().item(div.getChildNodes().getLength() - 1).getNodeValue().trim().compareToIgnoreCase(closeTag) == 0) {

				result.buffer = new StringBuffer();

				if (!sections.contains(WordSection.WordComment)) {
					result.word.comment = "";

					for (int i = 1; i < div.getChildNodes().getLength() - 1; i++)
						if (div.getChildNodes().item(i).getNodeName().compareToIgnoreCase("SPAN") == 0
								&& div.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE) {
							String lang = div.getChildNodes().item(i).getAttributes().item(0).getNodeValue();
							String word_comment = div.getChildNodes().item(i).getChildNodes().item(0).getNodeValue();
							result.buffer.append("word.comment[" + lang + "].value=" + word_comment + crlf);
							result.word.comment = (null != result.word.comment ? result.word.comment : "")
									+ (null != result.word.comment && result.word.comment.length() > 0 ? "," : "") + word_comment;
							if (!sections.contains(WordSection.WordComment))
								sections.add(WordSection.WordComment);
						}
				}
				verboseOut(result.buffer);
			}
		return result;
	}

	/**
	 * Parse the specified DIV node and if word Translation section found then
	 * fill the word with corresponding info
	 * 
	 * @param div
	 *            the DIV node to be parsed
	 * @param word
	 *            the Word containing the previous Word data parsed/found
	 * @return an object containing the previous Word + current word Translation
	 *         section data
	 */
	private ParseResult parseWordTranslation(Node div, Word word) {
		ParseResult result = new ParseResult(word);
		if (null != div
				&& null != div.getChildNodes()
				&& div.getChildNodes().getLength() >= 2
				&& div.getChildNodes().item(0).getNodeName().compareToIgnoreCase("B") == 0
				&& div.getChildNodes().item(1).getNodeName().compareToIgnoreCase("#text") == 0
				&& (div.getChildNodes().getLength() == 2 || div.getChildNodes().getLength() > 2
						&& div.getChildNodes().item(2).getNodeName().compareToIgnoreCase("SPAN") == 0)) {

			result.buffer = new StringBuffer();

			String lang = div.getChildNodes().item(0).getChildNodes().item(0).getAttributes().item(0).getNodeValue();
			String translation = div.getChildNodes().item(0).getChildNodes().item(0).getChildNodes().item(0).getNodeValue();

			if (sections.contains(WordSection.Translation))
				duplicate_sections.add(WordSection.Translation);

			result.buffer.append("translation[" + lang + "].value=" + translation + crlf);
			result.word.translation.add(new Translation("", translation));
			verboseOut(result.buffer);
			sections.add(WordSection.Translation);
		}
		return result;
	}

	/**
	 * If verbose=true then print out the info contained by out buffered string
	 * list
	 * 
	 * @param out
	 *            the buffer containing all the info collected during the
	 *            parsing phase that is to be printed out
	 */
	private void verboseOut(StringBuffer out) {
		if (verbose && null != out && out.length() > 0)
			System.out.print(out);
	}
}

